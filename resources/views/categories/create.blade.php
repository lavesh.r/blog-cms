@extends('layouts.admin-panel.app')

@section('content')

    <div class="card">
        <div class="card-header"><h2>CAdd NEw Category</h2></div>
        <div class="card-body">
            <form action="{{route('categories.store')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text"
                    class="form-control @error('name') is-invalid @enderror"
                    id="name"
                    value="{{old('name')}}"
                    name="name"
                    placeholder="Enter Category Name"
                    >
                    @error('name')
                        <p>{{$message}}</p>
                    @enderror
                </div>
                <button type="submit" class="btn btn-outline-success">Submit</button>
            </form>
        </div>
    </div>
@endsection
